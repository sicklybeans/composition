#!/bin/bash
# Basic idea:
# if git describe yields:
#   1.1.5-2
# This means the next potential commit will be 2 commits behind the version tagged with '1.1.5'
# By convention, the python version defined in constants.py should be:
#   1.1.6.dev2
# meaning that it is dev version #2 of prerelease for the next version (1.1.6)


if [[ "$1" == "--quiet" ]]; then
  flag_quiet="1"
elif [[ "$#" -ne "0" ]]; then
  echo "Incorrect usage" > /dev/stderr
  echo "Usage: $0 [--quiet]" > /dev/stderr
  exit 3
else
  flag_quiet="0"
fi

function errmsg {
  if [[ "$flag_quiet" == "0" ]]; then
    echo "Error: $1" > /dev/stderr
  fi
}
function assert_equal {
  if [[ "$1" != "$2" ]]; then
    errmsg "$3"
    exit 1
  fi
}

# Go to project directory
cd "$(dirname "$0")/.."

gver=$(git describe)
pver=$(./scripts/get_version.sh)

# First check that the versions are formatted correctly
if [[ ! $gver =~ ^[0-9]+\.[0-9]+\.[0-9]+(\-[0-9]+\-[a-z0-9]+)?$ ]]; then
  errmsg "git version '$gver' did not match expected regex"
  exit 2
elif [[ ! $pver =~ ^[0-9]+\.[0-9]+\.[0-9]+(\.dev[0-9]+)?$ ]]; then
  errmsg "project version '$pver' did not match expected regex"
  exit 2
fi

# Next extract the components of each version tag
gparts=($(echo "$gver" | sed -E 's/([0-9]+)\.([0-9]+)\.([0-9]+)(\-([0-9]+)\-.*)?/\1 \2 \3 \5/g'))
pparts=($(echo "$pver" | sed -E 's/([0-9]+)\.([0-9]+)\.([0-9]+)(\.dev([0-9]+))?/\1 \2 \3 \5/g'))

# If this is not a development version, just make sure the parts match
if [[ "${gparts[3]}" == "" ]]; then
  assert_equal "${gparts[0]}" "${pparts[0]}" "the major versions did not match '$gver' vs '$pver'"
  assert_equal "${gparts[1]}" "${pparts[1]}" "the minor versions did not match '$gver' vs '$pver'"
  assert_equal "${gparts[2]}" "${pparts[2]}" "the micro versions did not match '$gver' vs '$pver'"
  assert_equal "${gparts[3]}" "${pparts[3]}" "the devel versions did not match '$gver' vs '$pver'"
else
  # TODO: This currently wont work for minor version updates.
  assert_equal "${gparts[0]}" "${pparts[0]}" "the major versions did not match '$gver' vs '$pver'"
  assert_equal "${gparts[1]}" "${pparts[1]}" "the minor versions did not match '$gver' vs '$pver'"
  # For development version, the git tag should be one behind since it counts revisions *after* the last tag but the dev counts *before* release
  gmic="${gparts[2]}"
  ((testmicro=$gmic + 1))
  assert_equal "$testmicro" "${pparts[2]}" "the micro versions did not match '$gver' vs '$pver'"
  assert_equal "${gparts[3]}" "${pparts[3]}" "the devel versions did not match '$gver' vs '$pver'"
fi

exit 0
