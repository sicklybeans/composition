#!/bin/bash

set -e

# Go to project directory
cd "$(dirname "$0")/.."

# mypy actions entry point
mypy comp/application.py
