#!/bin/bash

# Go to project directory
cd "$(dirname "$0")/.."

pytest --cov=comp --cov-report html tests/
