import gi.repository
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, Gdk, GObject, GLib, Gio, WebKit2

import argparse
import pkgutil
import sys
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast


import comp.constants
from comp import action_service
from comp.base_model import Database
from comp.config import Config
from comp.models import Folder, Note, Node, NoteConnector
from comp.preferences_dialog import PreferencesDialog
from comp.components.main_window import MainWindow
from comp.components.note_editor import NoteEditor
from comp.components.note_viewer import NoteViewer
from comp.components.nav_pane import NavPane


class Application(Gtk.Application):
  main_window: MainWindow
  preferences_dialog: PreferencesDialog

  def __init__(self) -> None:
    Gtk.Application.__init__(self, application_id=comp.constants.APPID)

    self.main_window = MainWindow()
    self.preferences_dialog = PreferencesDialog()
    self.preferences_dialog.set_transient_for(self.main_window)

  def do_startup(self) -> None:
    Gtk.Application.do_startup(self)

    self.main_window.set_application(self)
    action_service.attach_application(self)

  def do_activate(self) -> None:
    self.main_window.present()

  def run(self, argv: List[str]=[]) -> int:
    return super().run(argv)


def main() -> int:
  parser = argparse.ArgumentParser(prog=comp.constants.APPNAME)
  parser.add_argument('-v', '--version', action='version', version='%(prog)s ' + comp.constants.__version__)
  args=parser.parse_args(sys.argv[1:])

  Config.initialize()
  Database.initialize()
  return Application().run()
