from __future__ import annotations

import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk

from typing import cast, ClassVar, Dict, Optional, List, Tuple


class _ActionService():
  _INSTANCE: ClassVar[Optional[_ActionService]] = None

  action_map: Dict[str, Tuple[Gio.SimpleAction, List[str]]]

  def __init__(self) -> None:
    self.action_map = {}
    _ActionService._INSTANCE = self


def _get() -> _ActionService:
  return _ActionService._INSTANCE or _ActionService()


def attach_application(application: Gtk.Application) -> None:
  for name, (action, accels) in _get().action_map.items():
    application.add_action(action)
    if accels:
      application.set_accels_for_action('app.' + name, accels)


def set_enabled(action_name: str, state: bool) -> None:
  act = _get().action_map.get(action_name, None)
  if not act:
    raise Exception('No action found with name: %s' % action_name)
  act[0].set_enabled(state)


def register(action: Gio.SimpleAction, accels: Optional[List[str]]=None) -> None:
  _get().action_map[action.get_name()] = (action, accels or [])
