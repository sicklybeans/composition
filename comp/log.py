import abc
import argparse
import inspect
import sys
import warnings
from typing import Any, Callable, ClassVar, Union, Iterable, Tuple, Optional, TypeVar, Dict

GLOBAL_LEVEL = 2
DEBUG_LEVEL = 0
INFO_LEVEL = 1
WARN_LEVEL = 2
ERROR_LEVEL = 3


_Type = TypeVar('_Type', bound=type)
class ClassLogger(object):
  _indent: ClassVar[str] = ''
  _low_prefix: ClassVar[str] = '\033[30;47m[low]\033[0m:'
  _msg_prefix: ClassVar[str] = '\033[30;102m[msg]\033[0m:'
  _wrn_prefix: ClassVar[str] = '\033[30;43m[wrn]\033[0m:'
  _err_prefix: ClassVar[str] = '\033[30;41m[err]\033[0m:'
  _max_class_length: ClassVar[int] = 5
  _max_method_length: ClassVar[int] = 5
  _msg_format_str: ClassVar[str]
  _indent_after_next: ClassVar[bool] = False
  _classname: Optional[str]
  _ignore: bool

  def __init__(self, ignore: bool=False, level: int=GLOBAL_LEVEL) -> None:
    self._ignore = ignore
    self._level = level
    self._classname = None

  def __call__(self, cls: _Type) -> _Type:
    self._classname = cls.__name__
    cls.log = self # type: ignore

    # find length of longest method of class, use this to set margins
    max_length = max([
      len(name) for name, attr in cls.__dict__.items() if callable(attr)
    ])
    ClassLogger._max_method_length = max(ClassLogger._max_method_length, max_length)
    ClassLogger._max_class_length = max(ClassLogger._max_class_length, len(self._classname))
    ClassLogger._msg_format_str = '%%-%ds: %%-%ds: %%s' % (ClassLogger._max_class_length, ClassLogger._max_method_length)
    return cls

  def _log(self, prefix: str, classname: str, funcname: str, msg: str, msg_args: Any=tuple()) -> None:
    msg = ClassLogger._indent + prefix + (
        ClassLogger._msg_format_str % (
            classname, funcname, (msg % msg_args)))
    print(msg)
    if ClassLogger._indent_after_next:
      ClassLogger._indent += '  '
      ClassLogger._indent_after_next = False

  def low(self, msg: str='', *msg_args: Any) -> None:
    if not self._ignore and self._level <= DEBUG_LEVEL:
      assert self._classname
      funcname = inspect.getouterframes(inspect.currentframe(), 2)[1][3]
      self._log(prefix=ClassLogger._low_prefix, classname=self._classname, funcname=funcname, msg=msg, msg_args=msg_args)

  def msg(self, msg: str='', *msg_args: Any) -> None:
    if not self._ignore and self._level <= INFO_LEVEL:
      assert self._classname
      funcname = inspect.getouterframes(inspect.currentframe(), 2)[1][3]
      self._log(prefix=ClassLogger._msg_prefix, classname=self._classname, funcname=funcname, msg=msg, msg_args=msg_args)

  def wrn(self, msg: str='', *msg_args: Any) -> None:
    if not self._ignore and self._level <= WARN_LEVEL:
      assert self._classname
      funcname = inspect.getouterframes(inspect.currentframe(), 2)[1][3]
      self._log(prefix=ClassLogger._wrn_prefix, classname=self._classname, funcname=funcname, msg=msg, msg_args=msg_args)

  def err(self, msg: str='', *msg_args: Any) -> None:
    if not self._ignore and self._level <= ERROR_LEVEL:
      assert self._classname
      funcname = inspect.getouterframes(inspect.currentframe(), 2)[1][3]
      self._log(prefix=ClassLogger._err_prefix, classname=self._classname, funcname=funcname, msg=msg, msg_args=msg_args)


def group_log(func: Callable) -> Callable:
  """
  Used in tests to decorate logged functions to produce nice indented output.

  Example:

    @group.log
    def method1(self, x):
      self.log.msg('x is %d' % x)
      self.method2( x * x )
      self.log.msg('leaving')

    @group_log
    def method2(self, y):
      self.log.msg('y is %d' % x)
      self.log.msg('leaving')

    >> self.method1(2)
    [log]: classname: method1 : x is 2
      [log]: classname : method2 : y is 4
        [log]: classname : method2 : leaving
      [log]: classname: method1 : leaving
  """
  def _func(*args: Any, **kwargs: Dict[str, Any]) -> Any:
    ClassLogger._indent_after_next = True
    try:
      return func(*args, **kwargs)
    finally:
      ClassLogger._indent = ClassLogger._indent[:-2]
  return _func
