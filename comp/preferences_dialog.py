import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gio, Gtk

import abc
import pkgutil
from typing import Any, Dict, TypeVar, Type, Union, Generic, List, cast

from comp.config import Config, SettingType, SettingBinding
from comp.log import ClassLogger
from comp import action_service

_SettingType = TypeVar('_SettingType', bound=SettingType)


class _ElementSettingBinding(abc.ABC, SettingBinding[_SettingType]):
  """
  Class representing a two-way binding between a Config setting and a Gtk.Widget.
  """
  def set_config_from_value(self) -> bool:
    """Changes Config based on state of widget and returns true if Config setting changed."""
    old = self.get_setting_value()
    new = self._get_value_from_element()
    if old != new:
      Config.get().set_setting(self._setting_name, new)
      return True
    return False

  def set_value_from_config(self) -> None:
    """Sets widget state based on Config setting value."""
    self._set_value_to_element(self.get_setting_value())

  @abc.abstractmethod
  def _get_value_from_element(self) -> _SettingType:
    pass

  @abc.abstractmethod
  def _set_value_to_element(self, value: _SettingType) -> None:
    pass


@Gtk.Template.from_string((pkgutil.get_data('views', 'preferences-dialog.ui') or bytes()).decode('UTF-8'))
@ClassLogger(ignore=False, level=0)
class PreferencesDialog(Gtk.Dialog):
  __gtype_name__ = 'PreferencesDialog'
  editor_word_wrap_checkbox = cast(Gtk.CheckButton, Gtk.Template.Child())
  style_background_color_entry = cast(Gtk.Entry, Gtk.Template.Child())
  style_foreground_color_entry = cast(Gtk.Entry, Gtk.Template.Child())
  style_code_color_entry = cast(Gtk.Entry, Gtk.Template.Child())
  style_body_width_entry = cast(Gtk.Entry, Gtk.Template.Child())
  style_code_indent_width_entry = cast(Gtk.Entry, Gtk.Template.Child())
  save_on_navigation_checkbox = cast(Gtk.CheckButton, Gtk.Template.Child())
  _config_setting_bindings: List[_ElementSettingBinding]

  def __init__(self) -> None:
    super().__init__()
    self._config_setting_bindings = [
      _EntryBinding('style.background-color', str, self.style_background_color_entry),
      _EntryBinding('style.foreground-color', str, self.style_foreground_color_entry),
      _EntryBinding('style.code-color', str, self.style_code_color_entry),
      _EntryBinding('style.body-width', int, self.style_body_width_entry),
      _EntryBinding('style.code-indent-width', int, self.style_code_indent_width_entry),
      _CheckBinding('editor-word-wrap', self.editor_word_wrap_checkbox),
      _CheckBinding('save-on-navigation', self.save_on_navigation_checkbox)
    ]

    # Setup actions
    action = Gio.SimpleAction.new('edit.preferences')
    action.set_enabled(True)
    action.connect('activate', self.show_dialog)
    action_service.register(action)

  def show_dialog(self, *_: Any) -> None:
    # set element values from config values
    for binding in self._config_setting_bindings:
      binding.set_value_from_config()
    self.show()

  @Gtk.Template.Callback('delete-event')
  def _on_delete(self, *_: Any) -> bool:
    """Intercepts 'delete' event, causing window to hide without being destroyed."""
    self._on_cancel_clicked()
    return True

  @Gtk.Template.Callback('cancel-clicked')
  def _on_cancel_clicked(self, *_: Any) -> None:
    self.hide()

  @Gtk.Template.Callback('save-clicked')
  def _on_save_clicked(self, *_: Any) -> None:
    # set config values from element values
    changed = False
    for binding in self._config_setting_bindings:
      if binding.set_config_from_value():
        changed = True

    if changed:
      Config.get().save()

    self.hide()


class _CheckBinding(_ElementSettingBinding[bool]):
  """
  Class enabling two-way binding between CheckButton and a boolean Config setting.
  """
  _element: Gtk.CheckButton

  def __init__(self, name: str, cb: Gtk.CheckButton) -> None:
    super().__init__(name, bool)
    self._element = cb

  def _get_value_from_element(self) -> bool:
    return self._element.get_active()

  def _set_value_to_element(self, value: bool) -> None:
    self._element.set_active(value)


class _EntryBinding(_ElementSettingBinding[_SettingType]):
  """
  Class enabling two-way binding between Entry and a Config setting.
  """
  _element: Gtk.Entry

  def __init__(self, setting_name: str, setting_type: Type[_SettingType], entry: Gtk.Entry) -> None:
    super().__init__(setting_name, setting_type)
    self._element = entry

  def _get_value_from_element(self) -> _SettingType:
    return cast(_SettingType, self._setting_type(self._element.get_text()))

  def _set_value_to_element(self, value: _SettingType) -> None:
    self._element.set_text(str(self.get_setting_value()))
