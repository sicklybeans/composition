import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GObject, GdkPixbuf

import pkgutil
from typing import Any, Dict, Optional, cast

from comp.base_model import Database
from comp.config import Config
from comp.log import ClassLogger
from comp.models import Folder, Note, Node, NoteConnector


_NOTE_ICON = Gtk.IconTheme.get_default().load_icon('text-x-generic-symbolic', 16, 0)
_FOLDER_ICON = Gtk.IconTheme.get_default().load_icon('folder-symbolic', 16, 0)


class _NavPaneModel(Gtk.TreeStore):
  __gsignals__ = {
    'node-title-changed': (GObject.SignalFlags.RUN_FIRST, None, ()),
  }

  def __init__(self) -> None:
    super().__init__(int, bool, str, GdkPixbuf.Pixbuf, bool)
    self.connect('row-changed', self._on_row_changed)

  def find_node_iter(self, node: Node) -> Optional[Gtk.TreeIter]:
    """
    Searches model for the given node. This is preferable to storing an iter map or path map because
    iters can become invalid and the path might not necessarily reflect drag/drop operations.

    This is definitely slower, but it's safer.

    Returns None on failure (breaks project's None/Exception convention, but it would be hard to
      implement a "has_node" method that's fast.)
    """
    def find_node_from_iter(itr_start: Optional[Gtk.TreeIter]) -> Optional[Gtk.TreeIter]:
      itr = itr_start
      while itr:
        if _NavPaneModel.row_to_object(self[itr]) == node:
          return itr
        itr = self.iter_next(itr)
      return None

    parent_id = node.get_parent_id()
    search_iter = None
    if parent_id:
      parent_itr = self.find_node_iter(Folder.get(parent_id))
      if parent_itr is None:
        return None
      search_itr = self.iter_children(parent_itr) # search in parent's iter's children
    else:
      search_itr = self.iter_children(None) # search in top level
    return find_node_from_iter(search_itr)

  def append_object(self, parent_itr: Optional[Gtk.TreeIter], obj: Node) -> Gtk.TreeIter:
    if isinstance(obj, Note):
      return self.append(parent_itr, [obj.id, True, obj.title, _NOTE_ICON, True])
    else:
      return self.append(parent_itr, [obj.id, False, obj.title, _FOLDER_ICON, True])

  @staticmethod
  def row_to_object(row: Gtk.TreeModelRow) -> Node:
    id = cast(int, row[0])
    is_note = cast(bool, row[1])
    if is_note:
      return Note.get(id)
    else:
      return Folder.get(id)

  def do_row_drop_possible(self, dest_path:Gtk.TreePath, _: Any) -> bool:
    """
    This is complicated, but I believe it's necessary.

    dest_path is the path of the row that the dropped row will be placed *above* (at same depth).
    However, we need to know if it's potential parent is a Folder. Thus, we have to extract the
    parent of `dest_path` (which might not exist and might not have a parent) and check if it's a
    Folder.
    """
    # if moving to top level, always allow
    if dest_path.get_depth() == 1:
      return True

    # get parent path - must make a copy or bad things happen
    parent = dest_path.copy()
    parent.up()

    try:
      parent_iter = self.get_iter(parent)
      return not self[parent_iter][1]  # return (not is_note)
    except ValueError:
      return False

  def rebuild_from_data(self) -> None:
    """
    TODO: need to clean this up by adding a "root" Folder instance.
    """
    self.clear()

    top_level_ids = set(Folder.get_loaded_ids())
    top_level_note_ids = set(Note.get_loaded_ids())
    for id in Folder.get_loaded_ids():
      folder = Folder.get(id)
      top_level_ids.difference_update(folder.child_folder_ids)
      top_level_note_ids.difference_update(folder.child_note_ids)

    def add_folder(parent_itr: Optional[Gtk.TreeIter], folder: Folder) -> None:
      itr = self.append_object(parent_itr, folder)
      for id in folder.child_note_ids:
        self.append_object(itr, Note.get(id))
      for id in folder.child_folder_ids:
        add_folder(itr, Folder.get(id))

    top_level_folders = [Folder.get(id) for id in top_level_ids]
    top_level_folders.sort(key=lambda folder: folder.title)
    for folder in top_level_folders:
      add_folder(None, folder)
    top_level_notes = [Note.get(id) for id in top_level_note_ids]
    top_level_notes.sort(key=lambda note: note.title)
    for note in top_level_notes:
      self.append_object(None, note)

  def _on_row_changed(self, model: Gtk.TreeStore, _: Gtk.TreePath, itr: Gtk.TreeIter) -> None:
    """
    This handler can be fired in two cases:
      1. the user edits the title of an object in the sidepane
      2. the user moves one of the rows in the sidepane
    The latter is complicated and fires a 'row-deleted' 'row-inserted' and 'row-changed' event.
    By testing, I've determined that only the 'row-changed' event is fired with a non-empty row.

    It should be noted that if you drag a folder into another folder, this method is fired once for
    the dragged folder and once for each of its children. From the perspective of the application's
    model, only the folder that was dragged should cause a Database update (via `change_parent`).
    Thus, we have to check if the row was actually changed.
    """
    row = model[itr]
    node = _NavPaneModel.row_to_object(row)

    # extract node title from TreeModelRow
    row_title = cast(str, row[2])
    # extract node parent_id from TreeModel
    row_parent_id = None
    parent_itr = model.iter_parent(itr)
    if parent_itr is not None:
      row_parent_id = cast(int, model[parent_itr][0])

    # check if TreeModelRow data different from Node data and if so, change Node and save
    has_change = False
    if row_title != node.title:
      has_change = True
      node.title = row_title
    if row_parent_id != node.get_parent_id():
      has_change = True
      node.change_parent(row_parent_id)
    Database.commit_changes()


@Gtk.Template.from_string((pkgutil.get_data('views', 'nav-pane.ui') or bytes()).decode('UTF-8'))
class NavPane(Gtk.TreeView):
  __gtype_name__ = 'NavPane'
  __gsignals__ = {
    'selected-node-changed': (GObject.SignalFlags.RUN_FIRST, None, ())
  }
  name_column = cast(Gtk.TreeViewColumn, Gtk.Template.Child())
  _nav_pane_model: _NavPaneModel
  _selected_node: Optional[Node]

  def __init__(self) -> None:
    super().__init__()
    self._selected_node = None
    self._nav_pane_model = _NavPaneModel()
    self._nav_pane_model.rebuild_from_data()
    self.set_model(self._nav_pane_model)

  def get_selected_node(self) -> Optional[Node]:
    return self._selected_node

  def add_new_node(self, node: Node) -> None:
    # get parent of current selection so we can insert new node at same level
    parent_itr, parent_id = None, None
    model, itr = self.get_selection().get_selected()
    assert model == self._nav_pane_model
    model = cast(_NavPaneModel, model)
    if itr:
      parent_itr = model.iter_parent(itr)
      parent_id = cast(int, model[parent_itr][0]) if parent_itr else None

    if parent_id:
      node.change_parent(parent_id)

    new_itr = model.append_object(parent_itr, node)
    # start editing title
    self.set_cursor(model.get_path(new_itr), self.name_column, True)

  def remove_node(self, node: Node) -> None:
    itr = self._nav_pane_model.find_node_iter(node)
    if not itr:
      raise Exception('Cannot remove node "%s" from tree: node not found in tree search' % node)
    self._nav_pane_model.remove(itr)

  @Gtk.Template.Callback('node-title-edited')
  def _on_node_title_edited(self, _: Any, path_str: str, new_name: str) -> None:
    self._nav_pane_model[path_str][2] = new_name #  triggers database.commit through row-changed listener

  @Gtk.Template.Callback('selection-changed')
  def _on_selection_changed(self, *_: Any) -> None:
    model, it = self.get_selection().get_selected()
    self._selected_node = _NavPaneModel.row_to_object(model[it]) if it else None
    self.emit('selected-node-changed')
