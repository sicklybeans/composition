import gi.repository
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import WebKit2, Gtk

import json
import mistune
from typing import Dict, Any, TypeVar, Type, Generic, Callable, Optional, Set, cast

from comp.config import Config, SettingType, SettingBinding
from comp.log import ClassLogger


_EMPTY_HTML = """<h1 style="text-align:center;">No note selected</h1>"""
_SettingType = TypeVar('_SettingType', bound=SettingType)
_pixel_parser = lambda width: '%dpx' % width


# See `_store_scroll_information`
_JAVASCRIPT_BLOCK = """
<script>
var body = document.documentElement.children[1];
function load_scroll_info() {
  if (!!document.title) {
    var scroll_info = JSON.parse(document.title);
    body.scrollTop = scroll_info.position;
  }
}
function store_scroll_info() {
  document.title = JSON.stringify({position: body.scrollTop});
}
load_scroll_info();
body.onscroll = store_scroll_info;
</script>
"""


class _CSSBinding(SettingBinding[_SettingType]):
  """Class binding a Config setting to CSS logic."""
  _selector: str
  _css_prop: str
  _parser: Optional[Callable[[_SettingType], str]]
  _remove_on_falsy: bool

  def __init__(
      self,
      setting_name: str,
      setting_type: Type[_SettingType],
      selector: str,
      css_prop: str,
      parser: Callable[[_SettingType], str]=None,
      remove_on_falsy: bool=True) -> None:
    super().__init__(setting_name, setting_type)
    self._selector = selector
    self._css_prop = css_prop
    self._parser = parser
    self._remove_on_falsy = remove_on_falsy

  def modify_style_map(self, css_map: Dict[str, Dict[str, str]]) -> None:
    rawval = self.get_setting_value()
    if not rawval and self._remove_on_falsy:
      if self._css_prop in css_map[self._selector]:
        css_map[self._selector].pop(self._css_prop)
    else:
      value = self._parser(rawval) if self._parser else str(rawval)
      css_map[self._selector][self._css_prop] = value


class NoteViewer(WebKit2.WebView):
  __gtype_name__ = 'NoteViewer'
  _bindings: Dict[str, _CSSBinding]
  _body_block: Optional[str]
  _markdown: mistune.Markdown
  _style_block: str
  _style_map: Dict[str, Dict[str, str]]
  _title_block: str

  def __init__(self) -> None:
    super().__init__()
    self._bindings = {}
    self._body_block = None
    self._markdown = mistune.Markdown()
    self._title_block = '<title></title>'
    self._style_block = ''
    self._style_map = {}
    for binding in [
      _CSSBinding('style.background-color', str, 'body', 'background-color'),
      _CSSBinding('style.foreground-color', str, 'body', 'color'),
      _CSSBinding('style.body-width', int, 'body > *', 'width', parser=_pixel_parser),
      _CSSBinding('style.code-indent-width', int, 'pre > code', 'margin-left', parser=_pixel_parser),
      _CSSBinding('style.code-color', str, 'code', 'color'),
    ]:
      binding = cast(_CSSBinding, binding)
      self._bindings[binding.get_setting_name()] = binding

    # prevent any GUI updates until after realized or you'll get a segfault
    def on_realize(_: NoteViewer) -> None:
      self.reset_scroll_state()
      self._reset_style_map()
      self._rebuild_style_block()
      Config.get().connect('setting-changed', self._on_config_changed)
      # see `_store_scroll_information` for explanation
      self.connect('notify::title', self._store_scroll_information)

    self.connect_after('realize', on_realize)

  def _on_config_changed(self, _: Any, name: str) -> bool:
    binding = self._bindings.get(name, None)
    if binding is not None:
      binding.modify_style_map(self._style_map)
      self._rebuild_style_block()
      return True # to indicate signal has been handled
    return False

  def reset_scroll_state(self) -> None:
    self._title_block = '<title>%s</title>' % json.dumps({'position': 0})

  def _store_scroll_information(self, *_: Any) -> None:
    """
    This is a hack to remember the scroll position between compiles of markdown.

    Basically, WebKit2.WebView operates as a separate process which controls its own scrollbar, DOM,
    and most other things. This means there is no way to extract information about the scroll
    position in pure python (that I have found).

    However, it is possible to get the title of the HTML shown from python. Thus, a hack was born:
      - Within web page, javascript code detects changes in the scroll position of <body> and sets
        the title of the web page with JSON encoded scroll information.
      - In python, we watch for changes to 'title' property and store it, thereby storing the scroll
        position.
      - Whenever we recompile the page, we set the title of the page with the previous title, which
        the javascript parses via JSON and uses to set the scroll position.
      - Whenever we show a new page, we reset the document title so that the scroll position resets.

    In theory, this could be extended to pass arbitrary information to/from web page.
    """
    if (title := self.get_title()):
      self._title_block = '<title>%s</title>' % title
    else:
      self.reset_scroll_state()

  def _reset_style_map(self) -> None:
    self._style_map = {
      'body': {},
      'body > *': {},
      'code': {},
      'pre > code': {'display': 'inline-block'}
    }
    for _, binding in self._bindings.items():
      binding.modify_style_map(self._style_map)

  def _rebuild_style_block(self) -> None:
    part_list = ['<style>']
    for css_class, style_map in self._style_map.items():
      part_list.append(css_class + '{')
      part_list.append('\n'.join([
        '  %s: %s;' % (prop, value) for prop, value in style_map.items()
      ]))
      part_list.append('}')
    part_list.append('</style>')
    self._style_block = '\n'.join(part_list)
    self._update()

  def _update(self) -> None:
    if self._body_block:
      self.load_html('<html><head>%s%s</head><body>%s%s</body></html>' % (
          self._title_block,
          self._style_block,
          self._body_block,
          _JAVASCRIPT_BLOCK))
    else:
      self.load_html(_EMPTY_HTML)

  def render_from_markdown(self, markdown_text: str) -> None:
    if not markdown_text:
      self._body_block = None
    else:
      self._body_block = self._markdown.render(markdown_text)
    self._update()
