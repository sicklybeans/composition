from __future__ import annotations
import gi.repository
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk, GObject, Gdk

import functools
import pkgutil
from typing import Any, List, Optional, Dict, Set, TypeVar, Type, Callable, Tuple, cast

import comp.constants
from comp.config import Config
from comp.log import ClassLogger
from comp.models import Note, NoteConnector


_T = TypeVar('_T')
_DEBUG_KEY_STROKES = False


@ClassLogger()
@Gtk.Template.from_string((pkgutil.get_data('views', 'note-editor.ui') or bytes()).decode('UTF-8'))
class NoteEditor(Gtk.TextView):
  __gtype_name__ = 'NoteEditor'
  __gsignals__ = {
    'content-changed': (GObject.SignalFlags.RUN_FIRST, None, ()),
    'is-modified-changed': (GObject.SignalFlags.RUN_FIRST, None, ()),
  }
  _is_modified: bool
  _ignore_content_change: bool
  _note: Optional[Note]
  log: ClassLogger

  def __init__(self) -> None:
    super().__init__()
    self._ignore_content_change = False # used to block updating '_is_modified' when changing shown note
    self._is_modified = False
    self._note = None
    self._buff_ops = NoteEditor.BufferOperations(self)
    self.get_buffer().connect('changed', self._on_note_content_change)

    # initialize settings from Config
    self._on_config_changed(None, 'editor-word-wrap')

    Config.get().connect('setting-changed', self._on_config_changed)

    self.connect('key-press-event', self._on_key_press_event)

  @property
  def is_modified(self) -> bool:
    return self._is_modified

  @is_modified.setter
  def is_modified(self, is_modified: bool) -> None:
    if self.is_modified == is_modified:
      return
    self._is_modified = is_modified
    self.emit('is-modified-changed')

  @property
  def note(self) -> Optional[Note]:
    return self._note

  @note.setter
  def note(self, note: Optional[Note]) -> None:
    if note is self.note:
      return
    self.log.low()

    # save previous note when navigating away
    if self.note is not None and self.is_modified:
      if Config.get().get_setting('save-on-navigation', bool):
        self.save_note()

    self._note = note
    self.is_modified = False  # emits is-modified-changed
    self.set_property('editable', self.note is not None)

    contents = NoteConnector.read_markdown_file(self.note) if self.note else ''
    # We need to block `_on_note_content_change` for two reasons:
    # 1. we don't want to set is_modified to `True` when setting content from fresh note
    # 2. when contents change from one str to another, the buffer actually fires 'changed' twice,
    #    first with empty string, then with new text. We only want to respond once
    self._ignore_content_change = True
    self.get_buffer().set_text(contents)
    self._ignore_content_change = False
    self.emit('content-changed')

  def get_contents(self) -> str:
    tb = self.get_buffer()
    return tb.get_text(tb.get_start_iter(), tb.get_end_iter(), True)

  def save_note(self) -> None:
    self.log.msg()
    if not self.note or not self.is_modified:
      raise ValueError()
    NoteConnector.write_markdown_file(self.note, self.get_contents())
    self.is_modified = False

  def _on_note_content_change(self, *_: Any) -> None:
    if not self._ignore_content_change:
      self.log.low()
      self.is_modified = True
      self.emit('content-changed')

  def _on_config_changed(self, _: Any, name: str) -> bool:
    if name == 'editor-word-wrap':
      word_wrap = Config.get().get_setting(name, bool)
      self.set_property('wrap-mode', Gtk.WrapMode.WORD if word_wrap else Gtk.WrapMode.NONE)
      return True # blocks signal from propagating
    return False

  def _on_key_press_event(self, _: NoteEditor, event_key: Gdk.EventKey) -> bool:
    _mod, kv = event_key.state, event_key.keyval
    mod = _mod or None

    if _DEBUG_KEY_STROKES and mod:
      print('Pressed key (%s, %s): ' % (str(mod.value_nicks), kv), end='')
    action = self._buff_ops.get_hotkey_action((mod, kv))
    if action:
      if _DEBUG_KEY_STROKES:
        print('found in map - responding')
      action()
      return True
    else:
      if _DEBUG_KEY_STROKES:
        print('not in map');
      return False

  class BufferOperations():
    _tb: Gtk.TextBuffer
    _keybind_map: Dict[Tuple[Optional[Gdk.ModifierType], int], Callable[[], None]]

    def get_hotkey_action(self, hotkey: Tuple[Optional[Gdk.ModifierType], int]) -> Optional[Callable[[],None]]:
      return self._keybind_map.get(hotkey, None)

    def __init__(self, ne: NoteEditor) -> None:
      primary = Gdk.ModifierType.CONTROL_MASK
      secondary = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD4_MASK | Gdk.ModifierType.SUPER_MASK
      primary_sh = Gdk.ModifierType.SHIFT_MASK | Gdk.ModifierType.CONTROL_MASK
      secondary_sh = Gdk.ModifierType.CONTROL_MASK | Gdk.ModifierType.MOD4_MASK | Gdk.ModifierType.SUPER_MASK | Gdk.ModifierType.SHIFT_MASK

      self._tb = ne.get_buffer()
      self._keybind_map = {}
      self._keybind_map[primary  , Gdk.KEY_u] = self._gen_nav_action(Gtk.TextIter.backward_visible_word_start)
      self._keybind_map[primary  , Gdk.KEY_p] = self._gen_nav_action(Gtk.TextIter.forward_visible_word_end)
      self._keybind_map[primary  , Gdk.KEY_j] = self._gen_nav_action(Gtk.TextIter.backward_char)
      self._keybind_map[primary  , Gdk.KEY_semicolon] = self._gen_nav_action(Gtk.TextIter.forward_char)
      self._keybind_map[secondary, Gdk.KEY_j] = self._gen_nav_action(Gtk.TextIter.set_line_offset, 0)
      self._keybind_map[secondary, Gdk.KEY_semicolon] = self._gen_nav_action(Gtk.TextIter.forward_to_line_end)
      self._keybind_map[primary  , Gdk.KEY_l] = self._gen_nav_line_action(-1)
      self._keybind_map[primary  , Gdk.KEY_k] = self._gen_nav_line_action(+1)
      self._keybind_map[secondary, Gdk.KEY_l] = self._gen_nav_line_action(-10)
      self._keybind_map[secondary, Gdk.KEY_k] = self._gen_nav_line_action(+10)
      # with shift
      self._keybind_map[primary_sh  , Gdk.KEY_U] = self._gen_nav_action(Gtk.TextIter.backward_visible_word_start, extend_selection=True)
      self._keybind_map[primary_sh  , Gdk.KEY_P] = self._gen_nav_action(Gtk.TextIter.forward_visible_word_end, extend_selection=True)
      self._keybind_map[primary_sh  , Gdk.KEY_J] = self._gen_nav_action(Gtk.TextIter.backward_char, extend_selection=True)
      self._keybind_map[primary_sh  , Gdk.KEY_colon] = self._gen_nav_action(Gtk.TextIter.forward_char, extend_selection=True)
      self._keybind_map[secondary_sh, Gdk.KEY_J] = self._gen_nav_action(Gtk.TextIter.set_line_offset, 0, extend_selection=True)
      self._keybind_map[secondary_sh, Gdk.KEY_colon] = self._gen_nav_action(Gtk.TextIter.forward_to_line_end, extend_selection=True)
      self._keybind_map[primary_sh  , Gdk.KEY_L] = self._gen_nav_line_action(-1, extend_selection=True)
      self._keybind_map[primary_sh  , Gdk.KEY_K] = self._gen_nav_line_action(+1, extend_selection=True)
      self._keybind_map[secondary_sh, Gdk.KEY_L] = self._gen_nav_line_action(-10, extend_selection=True)
      self._keybind_map[secondary_sh, Gdk.KEY_K] = self._gen_nav_line_action(+10, extend_selection=True)

      # Delete stuff
      self._keybind_map[primary  , Gdk.KEY_q] = self._gen_del_action(Gtk.TextIter.backward_visible_word_start)
      self._keybind_map[primary  , Gdk.KEY_r] = self._gen_del_action(Gtk.TextIter.forward_visible_word_end)
      self._keybind_map[primary  , Gdk.KEY_a] = self._gen_del_action(Gtk.TextIter.backward_char)
      self._keybind_map[primary  , Gdk.KEY_f] = self._gen_del_action(Gtk.TextIter.forward_char)
      self._keybind_map[secondary, Gdk.KEY_a] = self._gen_del_action(Gtk.TextIter.set_line_offset, 0)
      self._keybind_map[secondary, Gdk.KEY_f] = self._gen_del_action(Gtk.TextIter.forward_to_line_end)
      self._keybind_map[primary  , Gdk.KEY_d] = self._gen_del_line_action(-1)
      # self._keybind_map[primary  , Gdk.KEY_s] = self._gen_del_line_action(+1)
      self._keybind_map[secondary, Gdk.KEY_d] = self._gen_del_line_action(-10)
      self._keybind_map[secondary, Gdk.KEY_s] = self._gen_del_line_action(+10)

      # special
      self._keybind_map[None, Gdk.KEY_Return] = self.newline_indent
      self._keybind_map[None, Gdk.KEY_Tab] = self.insert_tab

    def _get_indent(self, line_number: int) -> int:
      itr = self._tb.get_iter_at_line(line_number)
      count = 0
      while itr.get_char() == ' ':
        count += 1
        itr.forward_char()
      return count

    def insert_tab(self) -> None:
      self._tb.insert_at_cursor('  ', 2)

    def newline_indent(self) -> None:
      itr = self._tb.get_iter_at_offset(self._tb.get_property('cursor-position'))
      line_number = itr.get_line()
      self._tb.insert_at_cursor('\n', 1) # insert newline first so indent doesn't count appending spaces
      indent = self._get_indent(line_number)
      self._tb.insert_at_cursor(' '*indent, indent)

    def _backward_to_line_start(self) -> None:
      itr = self._tb.get_iter_at_offset(self._tb.get_property('cursor-position'))
      itr.backward_line()

      self._tb.place_cursor(itr)

    def _gen_nav_action(self, iter_method: Callable, *args: Any, extend_selection: bool=False) -> Callable[[], None]:
      def action() -> None:
        itr = self._tb.get_iter_at_offset(self._tb.get_property('cursor-position'))
        iter_method(itr, *args)
        if not extend_selection:
          self._tb.place_cursor(itr)
        else:
          self._tb.move_mark_by_name('insert', itr)
      return action

    def _gen_del_action(self, iter_method: Callable, *args: Any) -> Callable[[], None]:
      def action() -> None:
        end_iter = self._tb.get_iter_at_offset(self._tb.get_property('cursor-position'))
        start_iter = end_iter.copy()
        iter_method(end_iter, *args)
        self._tb.delete(start_iter, end_iter)
      return action

    def _gen_nav_line_action(self, num_lines: int, extend_selection: bool=False) -> Callable[[], None]:
      def action()-> None:
        itr = self._tb.get_iter_at_offset(self._tb.get_property('cursor-position'))
        line_number = itr.get_line()
        line_offset = itr.get_line_offset()
        itr = self._tb.get_iter_at_line_offset(line_number + num_lines, line_offset)
        if not extend_selection:
          self._tb.place_cursor(itr)
        else:
          self._tb.move_mark_by_name('insert', itr)
      return action

    def _gen_del_line_action(self, num_lines: int) -> Callable[[], None]:
      def action() -> None:
        start_iter = self._tb.get_iter_at_offset(self._tb.get_property('cursor-position'))
        line_number = start_iter.get_line()
        line_offset = start_iter.get_line_offset()
        end_iter = self._tb.get_iter_at_line_offset(line_number + num_lines, line_offset)
        self._tb.delete(start_iter, end_iter)
      return action
