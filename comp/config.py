from __future__ import annotations  # allow forward references in annotations. remove in python4.0
import gi.repository
gi.require_version('Gtk', '3.0')
from gi.repository import GObject

import appdirs
import json
import os
import warnings
from typing import ClassVar, Dict, Set, Generic, TypeVar, Type, Tuple, Any, List, Callable, Optional, Iterator, Union

import comp.constants
import comp.file_op
from comp.constants import AlreadyInitializedError, NotInitializedError
from comp.log import ClassLogger


SettingType = Union[str, int, bool, float]
JSONDict = Dict[str, Any]

_CONFIG_NAME = 'config'
_ENV_LABEL_TEST = 'test'
_ENV_LABEL_NORMAL = 'normal'
_SettingType = TypeVar('_SettingType', bound=SettingType)
_file_parser = lambda filename: os.path.abspath(filename)


def _is_test_env() -> bool:
  return bool(os.getenv('VIRTUAL_ENV'))


def _get_default_data_directory() -> str:
  return appdirs.user_data_dir(appname=comp.constants.APPNAME)


class UnknownSettingError(ValueError):
  def __init__(self, name: str) -> None:
    super().__init__('Unknown setting: "%s"' % name)


class UnknownEnvironmentError(ValueError):
  def __init__(self, env_label: str) -> None:
    super().__init__('Unknown environment: "%s"' % env_label)


class CorruptError(Exception):
  def __init__(self, filename: str, reason: str):
    super().__init__('file "%s" is corrupt: %s' % (filename, reason))


class DuplicateSettingError(ValueError):
  pass


class SettingTypeError(TypeError):
  pass


class Setting(Generic[_SettingType]):
  _all_settings: Dict[str, Setting] = {}
  _name: str
  _default: Union[_SettingType, Callable[[], _SettingType]]
  _prop_type: Type[_SettingType]
  _parser: Optional[Callable[[_SettingType], _SettingType]]

  def __init__(self, name: str, prop_type: Type[_SettingType], default: Union[_SettingType, Callable[[], _SettingType]], parser: Callable[[_SettingType], _SettingType]=None) -> None:
    if name in self._all_settings:
      raise DuplicateSettingError('Multiple settings "%s"' % name)

    self._all_settings[name] = self
    self._default = default
    self._name = name
    self._parser = parser
    self._prop_type = prop_type

  def get_name(self) -> str:
    return self._name

  def get_type(self) -> Type[_SettingType]:
    return self._prop_type

  def parse(self, value: _SettingType) -> _SettingType:
    if self._parser:
      return self._parser(value)
    else:
      return value

  def get_default(self) -> _SettingType:
    if callable(self._default):
      return self.parse(self._default())
    else:
      return self.parse(self._default)


class SettingBinding(Generic[_SettingType]):
  """
  Abstract class representing a bound relationship between a Config setting and some other
  application component.
  """
  _setting_name: str
  _setting_type: Type[_SettingType]

  def __init__(self, setting_name: str, setting_type: Type[_SettingType]) -> None:
    prop = Config.get().get_settings().get(setting_name, None)
    if not prop:
      raise UnknownSettingError(setting_name)
    if prop.get_type() != setting_type:
      raise SettingTypeError()
    self._setting_name = setting_name
    self._setting_type = setting_type

  def get_setting_name(self) -> str:
    return self._setting_name

  def get_setting_value(self) -> _SettingType:
    return Config.get().get_setting(self._setting_name, self._setting_type)


@ClassLogger()
class Config(GObject.Object):
  __gsignals__ = {
    'setting-changed': (GObject.SignalFlags.RUN_FIRST, None, (object,)) #  args: prop name[str]
  }
  _INSTANCE: ClassVar[Optional[Config]] = None
  _settings: ClassVar[List[Setting]] = [
    Setting('data-directory', str, default=_get_default_data_directory, parser=_file_parser),
    Setting('editor-word-wrap', bool, default=False),
    Setting('save-on-navigation', bool, default=True),
    Setting('style.background-color', str, default=""),
    Setting('style.foreground-color', str, default=""),
    Setting('style.code-color', str, default=""),
    Setting('style.body-width', int, default=0),
    Setting('style.code-indent-width', int, default=25),
    Setting('_test-setting', str, 'default-value'),
  ]
  _setting_map: ClassVar[Dict[str, Setting]] = {
    prop.get_name(): prop for prop in _settings
  }
  log: ClassVar[ClassLogger]

  _config_filename: str
  _environment_label: str
  _environment_values: Dict[str, JSONDict]
  _is_test_env: bool

  def __init__(self, env_label: str) -> None:
    if Config._INSTANCE:
      raise Exception(Config.__class__.__name__)
    super(GObject.Object, self).__init__()
    Config._INSTANCE = self

    config_dir = appdirs.user_config_dir(appname=comp.constants.APPNAME)
    self._config_filename = os.path.join(config_dir, _CONFIG_NAME)
    self._environment_label = env_label
    self._environment_values = {}
    self._is_test_env = _is_test_env()

  @staticmethod
  def get() -> Config:
    if not Config._INSTANCE:
      raise NotInitializedError('Config must be initialied first')
    assert Config._INSTANCE
    return Config._INSTANCE

  @classmethod
  def get_settings(cls) -> Dict[str, Setting]:
    return {
      name: prop for name, prop in cls._setting_map.items()
    }

  @classmethod
  def initialize(cls, env_label: str=None, _mock_out_for_test: bool=False) -> None:
    """
    Initializes config for app by reading config file if it exists.

    If the config file does not exist, this simply prints a message and returns and sets all
    setting values to their default settings.

    Raises:
      PermissionError: if config file exists but cannot be read
      CorruptError: if config file exists but contains an error
    """
    cls.log.low('initializing config with environment: %s', env_label)

    # Check that singleton hasn't already been initialized
    if Config._INSTANCE != None:
      raise AlreadyInitializedError('Config already initialized!')

    # Create singleton instance
    env_label = env_label or (_ENV_LABEL_TEST if _is_test_env() else _ENV_LABEL_NORMAL)
    config = Config._INSTANCE = Config(env_label)

    # Initialize environments with default values
    for env in {_ENV_LABEL_NORMAL, _ENV_LABEL_TEST}:
      config._set_environment_from_defaults(env)

    # Override defaults with values loaded from config file
    try:
      config._reload()
    except FileNotFoundError as ex:
      print('No config file found. Using default values')
      cls.log.msg('No config file found. Using default values')
    if config._environment_label not in config._environment_values:
      raise UnknownEnvironmentError(config._environment_label)

  @classmethod
  def deinitialize(cls) -> None:
    """Undoes initialization. This is only for testing purposes!"""
    warnings.warn('This method should only be used in tests!')
    Config._INSTANCE = None

  def _set_environment_from_defaults(self, env_label: str) -> None:

    value_dict = self._environment_values[env_label] = {}
    for name, prop in Config._setting_map.items():
      value_dict[name] = prop.get_default()

  def _reload(self) -> None:
    self.log.low('loading config file "%s"', self._config_filename)
    assert self._config_filename

    try:
      contents = comp.file_op.read_file(self._config_filename)
      raw_dict = json.loads(contents)
    except json.decoder.JSONDecodeError:
      raise CorruptError(self._config_filename, 'Improper JSON')

    expected_keys = set(self._setting_map.keys())
    for env_label, env_dict in raw_dict.items():
      if not isinstance(env_dict,  dict):
        raise CorruptError(self._config_filename, 'Expected key "%s" to have dict as value' % env_label)
      self._set_environment_from_defaults(env_label)

      for key, value in env_dict.items():
        try:
          self.set_setting(key, value, env_label=env_label)
        except UnknownSettingError:
          raise CorruptError(self._config_filename, 'Unexpected setting name "%s" in environment "%s"' % (key, env_label))
        except SettingTypeError:
          raise CorruptError(self._config_filename, 'Unexpected type "%s" for setting "%s" in environment "%s"' % (type(key), key, env_label))

  def is_test_env(self) -> bool:
    return self._is_test_env

  def get_setting(self, name: str, type: Type[_SettingType], env_label: str=None) -> _SettingType:

    env_label = env_label or self._environment_label
    if name not in self._setting_map:
      raise UnknownSettingError(name)
    if env_label not in self._environment_values:
      raise UnknownEnvironmentError(env_label)

    value = self._environment_values[env_label][name]
    if not isinstance(value, type):
      raise SettingTypeError('Invalid setting type: %s, %s' % (name, type))
    return value

  def set_setting(self, name: str, value: Any, env_label: str=None) -> None:
    env_label = env_label or self._environment_label
    if name not in self._setting_map:
      raise UnknownSettingError(name)
    if env_label not in self._environment_values:
      raise UnknownEnvironmentError(env_label)

    prop = self._setting_map[name]
    value = prop.parse(value)
    if not isinstance(value, prop.get_type()):
      raise SettingTypeError('Invalid setting type: %s, %s' % (name, prop.get_type()))
    self._environment_values[env_label][name] = value

    # only emit signal if the setting value changed for the currently used environment.
    if env_label == self._environment_label:
      self.emit('setting-changed', name)

  def get_environments(self) -> Set[str]:
    return set(self._environment_values.keys())

  def save(self) -> None:
    self.log.msg('saving config file: %s' % self._config_filename)
    contents = json.dumps(self._environment_values, indent=2)
    comp.file_op.write_file(self._config_filename, contents)

  def get_database_dir(self) -> str:
    return os.path.join(self.get_setting('data-directory', str), 'database')

  def get_note_markdown_file(self, id: int) -> str:
    return os.path.join(self.get_setting('data-directory', str), 'notes_markdown', '%d.md' % id)

  def get_record_filename(self, table_name: str, id: int) -> str:
    return os.path.join(self.get_database_dir(), table_name, '%d.info' % id)

  def get_record_list_filename(self, table_name: str) -> str:
    return os.path.join(self.get_database_dir(), table_name, 'master.record')
