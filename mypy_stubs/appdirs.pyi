"""
Stub for appdirs
"""


def user_data_dir(appname: str=None, appauthor: str=None, version: str=None, roaming: bool=False) -> str:
  pass

def user_config_dir(appname: str=None, appauthor: str=None, version: str=None, roaming: bool=False) -> str:
  pass
