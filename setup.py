#!/usr/bin/env python3.8
import os
import sys
import subprocess
import setuptools
from setuptools import setup

here = os.path.abspath(os.path.dirname(__file__))

# TODO: This is a hack that works around the fact that setuptools does not work
# with pycairo and PyGObject. When you put the requirements given in
# requirements.txt into the `install_requires`, python setup.py install will
# fail at the PyGObject stage because setuptools installs PyGObject without
# installing pycairo first (for some odd reason). This may only occur in
# virtual environments.
# TODO: This should eventually be fixed for two reasons:
#  1. It requires pip to be installed on user's system
#  2. It assumes that pip3 uses the same python version as the version they are
#     installing this app onto.
# Both of these are bad and brittle
REQUIREMENTS_FILE = os.path.join(here, 'requirements.txt')
subprocess.call(['pip3', 'install', '-r', REQUIREMENTS_FILE])

exec(open('comp/constants.py').read())

if sys.version_info[0] != 3:
    print('This script requires Python 3')
    exit(1)

setup(
    name='comp',
    version=__version__,
    python_requires='>=3',
    packages=setuptools.find_packages(),
    include_package_data=True,
    data_files=[
      ('views', [
        'static/views/preferences-dialog.ui',
        'comp/components/main-window.ui',
        'comp/components/nav-pane.ui',
        'comp/components/note-editor.ui',
        'static/views/__init__.py'
        ]),
      ('css', [
        'static/css/application.css',
        'static/css/__init__.py'
        ])
      ],
    entry_points={
        'console_scripts': [
            'comp=comp.application:main'
        ],
    }
)
