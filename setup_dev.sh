# THIS SCRIPT MUST BE SOURCED
# This script does four things
# 1. Creates a pip-enabled virtualenv via pipenv
# 2. Creates test data environment and copies standard testing input data
# 3. Sets environment flags so that the application reads from test data
# 4. Sets up git hooks so commit, etc work correctly

THIS_SCRIPT="setup_dev.sh"
export _PROJECT_DIR="$PWD"

# Set MYPY dir so that mypy can see my custom stubs
export MYPYPATH="$_PROJECT_DIR/mypy_stubs"

function reinstall {
  ./scripts/reinstall.sh
}

# These functions serve as pseudo-scripts we can call to turn the test
# environment on/off
function dev-env-on {
  cd "$_PROJECT_DIR"

  export -f reinstall

  # Activate virtual environment (will not return until the venv is exited)
  pipenv shell
}

# Use while loop so we can break out without exiting a sourced script.
flag_success="0"
while [[ "1" == "1" ]]; do

  # Check that script is sourced.
  if [[ ! $0 =~ bash$ ]] && [[ ! $0 =~ sh$ ]] && [[ ! $0 =~ zsh$ ]]; then
    echo "Error: this script must be sourced (from bash, sh, or zsh)" > /dev/stderr
    break
  fi

  # Ensure we are in the project directory by checking for the '.git'
  if [[ ! -d "$PWD/.git" ]] || [[ ! -f "$PWD/$THIS_SCRIPT" ]]; then
    echo "Error: Please source this script from the project directory" > /dev/stderr
    break
  fi

  # export all the important stuff
  export _PROJECT_DIR

  # Setup git development hooks.
  echo "Setting up git hooks"
  ./scripts/setup_git_hooks
  if [[ "$?" -ne "0" ]]; then break; fi

  flag_success="1"
  break
done

if [[ "$flag_success" -eq "1" ]]; then
  echo "Dev environment setup was successful."
  echo ""
  echo "Guide:"
  echo "  dev-env-on             : enter dev environment"
  echo "  exit                   : exits dev environment"
  echo "  python setup.py install: installs project"
  true # set $? to 0
else
  echo "Something went wrong while setting up the dev environment" > /dev/stderr
  false # set $? to 1
fi