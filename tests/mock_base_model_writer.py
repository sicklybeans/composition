import json
from typing import Any, Type, Dict, Union, Set
from comp.base_model import BaseModel, Database, NotRegisteredError, Field, LoadingStrategy


TableClass = Type[BaseModel]
ID = int
JSONDict = Dict[str, Any]


class _FakeBaseModelWriter(object):
  """
  Realistic stand in for BaseModelWriter for use in testing.

  Some features:
    - given map of table classes and obj dicts, returns realistic record list file for each
      table_class and realistic record dict for each given obj dict
    - by default, `read` methods raise FileNotFoundError for any table class/id combination not
      explicitly given in initialization
    - the write methods permanently change the return values of the read methods to correspond to
      what would actually be read given that write actually occurred
  Cons:
    - very tight coupling with actual implementation of BaseModelWriter : (
  """
  _read_record_list_respones: Dict[TableClass, Union[Set[int], Exception]]
  _read_record_respones: Dict[TableClass, Dict[ID, Union[JSONDict, Exception]]]
  _default_read_record_list_response: Union[Set[int], Exception]
  _default_read_record_response: Union[JSONDict, Exception]

  def __init__(
      self,
      database_data: Dict[TableClass, Union[Dict[ID, Union[JSONDict, Exception]], Exception]],
      default_record_response=FileNotFoundError(),
      default_record_list_response=FileNotFoundError()) -> None:
    self._read_record_list_respones = {}
    self._read_record_respones = {}
    self._default_read_record_list_response = default_record_list_response
    self._default_read_record_response = default_record_response

    for table_class, response in database_data.items():
      if isinstance(response, dict):
        self._read_record_respones[table_class] = response
        self._read_record_list_respones[table_class] = set([id for id in response.keys()])
      elif isinstance(response, Exception):
        self._read_record_list_respones[table_class] = response
      else:
        raise Exception('Bad database_data value: %s (type=%s)' % (str(response), str(type(response))))

  def read_record_list_file(self, table_class: TableClass) -> Set[int]:
    response = self._read_record_list_respones.get(table_class, self._default_read_record_list_response)
    if isinstance(response, Exception):
      raise response
    else:
      return response

  def read_record_file(self, table_class: TableClass, id: int) -> JSONDict:
    response = self._default_read_record_response
    try:
      response = self._read_record_respones[table_class][id]
    except KeyError:
      pass

    if isinstance(response, Exception):
      raise response
    else:
      return response

  def write_record_list_file(self, table_class: TableClass, ids: Set[int]) -> None:
    self._read_record_list_respones[table_class] = set([id for id in ids])

  def write_record_file(self, table_class: TableClass, id: int, obj_dict: JSONDict) -> None:
    contents = json.dumps(obj_dict, indent=2)
    self._read_record_respones[table_class][id] = json.loads(contents)


def mock_base_model_writer(mock_writer, database_data):
  fake_writer = _FakeBaseModelWriter(database_data)
  mock_writer.read_record_file.side_effect = fake_writer.read_record_file
  mock_writer.read_record_list_file.side_effect = fake_writer.read_record_list_file
  mock_writer.write_record_file.side_effect = fake_writer.write_record_file
  mock_writer.write_record_list_file.side_effect = fake_writer.write_record_list_file
