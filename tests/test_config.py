import copy
from pytest import raises, fixture
from unittest import mock

import comp.constants
from comp.constants import AlreadyInitializedError, NotInitializedError
from comp.config import *


def _set_is_test_environment(is_test):
  if is_test:
    os.environ['VIRTUAL_ENV'] = '/home/user/.local/share/virtualenvs/something'
  else:
    if 'VIRTUAL_ENV' in os.environ:
      del os.environ['VIRTUAL_ENV']


@fixture(autouse=True)
def global_setup_teardown():
  _orig_env_vars = copy.deepcopy(os.environ)
  _set_is_test_environment(False)

  yield

  Config._INSTANCE = None
  for key, value in _orig_env_vars.items():
    os.environ[key] = value


class TestConfig(object):

  @mock.patch('comp.file_op.read_file', return_value='{}')
  def test_init_basic(self, _):
    Config.initialize()
    assert isinstance(Config.get(), Config)

  @mock.patch('comp.file_op.read_file', return_value='{}')
  def test_init_singleton_exception(self, _):
    Config.initialize()
    with raises(Exception):
      Config('normal')

  @mock.patch('comp.file_op.read_file')
  @mock.patch('appdirs.user_config_dir', return_value='fake/config/dir')
  def test_initialize_basic(self, mock_user_config_dir, mock_read_file):
    mock_read_file.return_value = '{}'
    Config.initialize()
    mock_read_file.assert_called_once_with('fake/config/dir/config')
    mock_user_config_dir.assert_called_once_with(appname=comp.constants.APPNAME)

  @mock.patch('comp.file_op.read_file')
  def test_initialize_double_call(self, mock_read_file):
    """
    Calling initialize twice should raise an error.
    """
    mock_read_file.return_value = '{}'
    Config.initialize()
    with raises(AlreadyInitializedError):
      Config.initialize()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_uses_defaults(self, mock_read_file):
    """
    When config is empty, all settings should have their default values
    """
    mock_read_file.return_value = '{}'
    Config.initialize()

    for name, prop in Config.get().get_settings().items():
      assert Config.get().get_setting(name, prop.get_type()) == prop.get_default()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_explicit_new_environment(self, mock_read_file):
    """
    When env_label is given explicitly, get_setting returns those values
    """
    mock_read_file.return_value = json.dumps({
      'fake-environment': {'_test-setting': 'non-default'}
    })
    Config.initialize(env_label='fake-environment')

    # Ensure all values are default except 'test-setting' which takes on 'fake-environment' value
    assert Config.get().get_setting('_test-setting', str) == 'non-default'
    for name, prop in Config.get_settings().items():
      if name != '_test-setting':
        assert Config.get().get_setting(prop.get_name(), prop.get_type()) == prop.get_default()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_explicit_new_environment_bad(self, mock_read_file):
    """
    When env_label is given explicitly but not valid, expect error
    """
    mock_read_file.return_value = json.dumps({'fake-environment': {'_test-setting': 'non-default'}})
    with raises(UnknownEnvironmentError):
      Config.initialize(env_label='fake-environment-2')

  @mock.patch('comp.file_op.read_file')
  def test_initialize_complex_config(self, mock_read_file):
    mock_read_file.return_value = json.dumps({
      'normal': {'_test-setting': 'normal value'},
      'fake-environment-1': {'_test-setting': 'fake1 value'}
    })
    Config.initialize(env_label='fake-environment-1')

    assert Config.get().get_setting('_test-setting', str, env_label='test') == 'default-value'
    assert Config.get().get_setting('_test-setting', str, env_label='normal') == 'normal value'
    assert Config.get().get_setting('_test-setting', str, env_label='fake-environment-1') == 'fake1 value'

    # Ensure all other values in all environments are default
    for env in ['test', 'normal', 'fake-environment-1']:
      for name, prop in Config.get_settings().items():
        if prop.get_name() != '_test-setting':
          assert Config.get().get_setting(prop.get_name(), prop.get_type(), env_label=env) == prop.get_default()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_config_bad_structure(self, mock_read_file):
    """
    Config contains a setting as top level definition
    """
    mock_read_file.return_value = json.dumps({
      '_test-setting': 'should not be top level'
    })
    with raises(CorruptError):
      Config.initialize()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_config_setting_type_mismatch(self, mock_read_file):
    """
    Config contains a value of wrong type
    """
    mock_read_file.return_value = json.dumps({
      'normal': {'_test-setting': 10}
    })
    with raises(CorruptError):
      Config.initialize()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_config_bad_setting(self, mock_read_file):
    """
    Config contains an unknown setting
    """
    mock_read_file.return_value = json.dumps({
      'normal': {'fake setting': 'hi'}
    })
    with raises(CorruptError):
      Config.initialize()

  @mock.patch('comp.file_op.read_file')
  def test_initialize_invalid_json(self, mock_read_file):
    """
    When comp.file_op.read_file raises a FileFormatError
    """
    mock_read_file.return_value = '{ "invalid": "json"'
    with raises(CorruptError):
      Config.initialize()

  @mock.patch('comp.file_op.read_file', side_effect=FileNotFoundError())
  def test_initialize_missing_file(self, mock_read_file):
    """
    When comp.file_op.read_file raises a FileNotFoundError should still use default values.
    """
    Config.initialize()

    # Ensure all other values in all environments are default
    for env in ['test', 'normal']:
      for name, prop in Config.get_settings().items():
        assert Config.get().get_setting(prop.get_name(), prop.get_type(), env_label=env) == prop.get_default()

  @mock.patch('comp.file_op.read_file')
  def test_get_setting(self, mock_read_file):
    _set_is_test_environment(False)
    mock_read_file.return_value = json.dumps({
      'normal': {'_test-setting': 'value-1'},
    })
    Config.initialize()
    assert Config.get().get_setting('_test-setting', str) == 'value-1'

  @mock.patch('comp.file_op.read_file')
  def test_get_setting_normal_environment(self, mock_read_file):
    """
    When OS variables indicate a prod environment, get_setting uses 'normal' values
    """
    _set_is_test_environment(False)
    mock_read_file.return_value = json.dumps({
      'normal': {'_test-setting': 'new-normal-value'},
      'test': {'_test-setting': 'new-test-value'},
    })
    Config.initialize()

    # Ensure all values are default except 'test-setting' which takes on 'normal' value
    assert Config.get().get_setting('_test-setting', str) == 'new-normal-value'
    for name, prop in Config.get_settings().items():
      if prop.get_name() != '_test-setting':
        assert Config.get().get_setting(prop.get_name(), prop.get_type()) == prop.get_default()

  @mock.patch('comp.file_op.read_file')
  def test_get_setting_test_environment(self, mock_read_file):
    """
    When OS variables indicate a testing environment, get_setting uses 'test' values
    """
    _set_is_test_environment(True)
    mock_read_file.return_value = json.dumps({
      'normal': {'_test-setting': 'new-normal-value'},
      'test': {'_test-setting': 'new-test-value'},
    })
    Config.initialize()

    # Ensure all values are default except 'test-setting' which takes on 'normal' value
    assert Config.get().get_setting('_test-setting', str) == 'new-test-value'
    for name, prop in Config.get_settings().items():
      if prop.get_name() != '_test-setting':
        assert Config.get().get_setting(prop.get_name(), prop.get_type()) == prop.get_default()

  @mock.patch('comp.file_op.read_file')
  def test_get_setting_invalid_setting(self, mock_read_file):
    _set_is_test_environment(False)
    mock_read_file.return_value = '{}'
    Config.initialize()
    with raises(UnknownSettingError) as ex:
      Config.get().get_setting('invalid setting', str)

  @mock.patch('comp.file_op.read_file')
  def test_get_setting_invalid_environment(self, mock_read_file):
    _set_is_test_environment(False)
    mock_read_file.return_value = '{}'
    Config.initialize()
    with raises(UnknownEnvironmentError) as ex:
      Config.get().get_setting('_test-setting', str, env_label='invalid environment')

  @mock.patch('comp.file_op.read_file')
  def test_get_setting_wrong_type(self, mock_read_file):
    """
    When get_setting is passed wrong type, it catches type mismatch.
    """
    _set_is_test_environment(False)
    mock_read_file.return_value = '{}'
    Config.initialize()
    Config.get().get_setting('_test-setting', str)
    with raises(SettingTypeError) as ex:
      Config.get().get_setting('_test-setting', int)

  @mock.patch('comp.file_op.read_file')
  def test_get_setting_environment_specifier(self, mock_read_file):
    """
    When get_setting is passed an env_label, return values specific to that environment.
    """
    _set_is_test_environment(False)
    mock_read_file.return_value = json.dumps({
      'fake-environment': {'_test-setting': 'non-default'}
    })
    Config.initialize()
    # Normal environment unchanged
    assert Config.get().get_setting('_test-setting', str) == 'default-value'
    # New environment changed
    assert Config.get().get_setting('_test-setting', str, env_label='fake-environment') == 'non-default'

  @mock.patch('comp.file_op.read_file')
  def test_set_setting(self, mock_read_file):
    """
    set_setting updates setting value
    """
    mock_read_file.return_value = json.dumps({})
    Config.initialize()
    assert Config.get().get_setting('_test-setting', str) == 'default-value'
    Config.get().set_setting('_test-setting', 'new-value')
    assert Config.get().get_setting('_test-setting', str) == 'new-value'

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_emits_setting_change(self, mock_read_file):
    """
    set_setting updates setting value
    """
    mock_read_file.return_value = json.dumps({})
    mock_listener = mock.Mock()
    Config.initialize()
    Config.get().connect('setting-changed', mock_listener)
    Config.get().set_setting('_test-setting', 'new-value')
    mock_listener.assert_called_once_with(Config.get(), '_test-setting')

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_environment_specifier(self, mock_read_file):
    """
    When set_setting passed env_label, only changes values for that env
    """
    mock_read_file.return_value = json.dumps({'fake-environment': {}})
    Config.initialize()
    Config.get().set_setting('_test-setting', 'new-value', env_label='fake-environment')
    assert Config.get().get_setting('_test-setting', str) == 'default-value'
    assert Config.get().get_setting('_test-setting', str, env_label='fake-environment') == 'new-value'

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_environment_specifier_no_emit(self, mock_read_file):
    """
    When set_setting passed env_label, does not emit setting-changed
    """
    mock_read_file.return_value = json.dumps({'fake-environment': {}})
    mock_listener = mock.Mock()
    Config.initialize()
    Config.get().connect('setting-changed', mock_listener)
    Config.get().set_setting('_test-setting', 'new-value', env_label='fake-environment')
    mock_listener.assert_not_called()
    Config.get().set_setting('_test-setting', 'new-value')
    mock_listener.assert_called_once_with(Config.get(), '_test-setting')

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_uses_explicit_environment(self, mock_read_file):
    """
    set_setting modifies environment passed to initialize by default
    """
    mock_read_file.return_value = json.dumps({'fake-environment': {}})
    mock_listener = mock.Mock()
    Config.initialize(env_label='fake-environment')
    Config.get().connect('setting-changed', mock_listener)
    assert Config.get().get_setting('_test-setting', str) == 'default-value'
    Config.get().set_setting('_test-setting', 'new-value')
    assert Config.get().get_setting('_test-setting', str) == 'new-value'
    assert Config.get().get_setting('_test-setting', str, env_label='test') == 'default-value'
    mock_listener.assert_called_once()

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_invalid_setting(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    mock_read_file.return_value = '{}'
    Config.initialize()
    with raises(UnknownSettingError):
      Config.get().set_setting('bad-setting', 'new-value')

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_invalid_environment(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    mock_read_file.return_value = '{}'
    Config.initialize()
    Config.get().set_setting('_test-setting', 'new-value')
    with raises(UnknownEnvironmentError):
      Config.get().set_setting('_test-setting', 'new-value', env_label='bad-environment')

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_wrong_type(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    mock_read_file.return_value = '{}'
    Config.initialize()
    with raises(SettingTypeError):
      Config.get().set_setting('_test-setting', 10)

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_invalid_setting_no_emit(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    mock_read_file.return_value = '{}'
    mock_listener = mock.Mock()
    Config.initialize()
    Config.get().connect('setting-changed', mock_listener)
    with raises(UnknownSettingError):
      Config.get().set_setting('bad-setting', 'new-value')
    mock_listener.assert_not_called()

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_invalid_environment_no_emit(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    mock_read_file.return_value = '{}'
    mock_listener = mock.Mock()
    Config.initialize()
    Config.get().connect('setting-changed', mock_listener)
    with raises(UnknownEnvironmentError):
      Config.get().set_setting('_test-setting', 'new-value', env_label='bad-environment')
    mock_listener.assert_not_called()

  @mock.patch('comp.file_op.read_file')
  def test_set_setting_wrong_type_no_emit(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    mock_read_file.return_value = '{}'
    mock_listener = mock.Mock()
    Config.initialize()
    Config.get().connect('setting-changed', mock_listener)
    with raises(SettingTypeError):
      Config.get().set_setting('_test-setting', 10)
    mock_listener.assert_not_called()

  @mock.patch('comp.file_op.read_file')
  def test_is_test_env_true(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    _set_is_test_environment(True)
    mock_read_file.return_value = '{}'
    Config.initialize()
    assert Config.get().is_test_env()

  @mock.patch('comp.file_op.read_file')
  def test_is_test_env_false(self, mock_read_file):
    """
    Raises exception when passed invalid setting name
    """
    _set_is_test_environment(False)
    mock_read_file.return_value = '{}'
    Config.initialize()
    assert not Config.get().is_test_env()

  def test_must_be_initialized(self):
    with raises(NotInitializedError):
      Config.get()

  @mock.patch('comp.file_op.write_file', autospec=True)
  @mock.patch('comp.file_op.read_file', autospec=True)
  @mock.patch('appdirs.user_config_dir', autospec=True)
  def test_saves_all_props_all_envs(self, mock_config_dir, mock_read, mock_write):
    _set_is_test_environment(True)
    mock_config_dir.return_value = '/fake/config/dir'
    mock_read.return_value = json.dumps({
      'normal': {'_test-setting': 'new-normal-value'},
      'test': {'_test-setting': 'new-test-value'},
    })
    Config.initialize()

    Config.get().set_setting('_test-setting', 'newer-test-value')
    Config.get().set_setting('_test-setting', 'newer-normal-value', env_label='normal')

    # test saving by comparing dict of settings before save to json.loads of saved contents
    expected_dict = {
      'normal': {
        name: Config.get().get_setting(name, p.get_type(), env_label='normal')
        for name, p in Config.get_settings().items()
      },
      'test': {
        name: Config.get().get_setting(name, p.get_type(), env_label='test')
        for name, p in Config.get_settings().items()
      }
    }

    mock_write.assert_not_called()
    Config.get().save()
    mock_write.assert_called_once()

    filename, contents = mock_write.mock_calls[0][1]
    assert filename == '/fake/config/dir/config'
    assert json.loads(contents) == expected_dict

  @mock.patch('comp.file_op.read_file', autospec=True)
  def test_get_environments(self, mock_read):
    mock_read.return_value = json.dumps({
      'normal': {},
      'new-env-1': {},
      'new-env-2': {'_test-setting': 'value'}
    })
    Config.initialize()

    assert Config.get().get_environments() == {
      'normal', 'new-env-1', 'new-env-2', 'test'
    }

  class TestSettings(object):
    """Tests values and settings for specific settings"""

    def test_all_settings(self):
      prop_names = set(Config.get_settings().keys())
      assert len(prop_names) == 9

    def test_duplicate_setting(self):
      Setting('some-setting', int, default=0)
      with raises(DuplicateSettingError):
        Setting('some-setting', int, default=0)

    def test_data_directory_basic(self):
      prop = Config.get_settings()['data-directory']
      assert prop.get_name() == 'data-directory'
      assert prop.get_type() == str
      assert prop.parse('/something//other/../other') == '/something/other'

      # check it returns appdirs default
      with mock.patch('appdirs.user_data_dir', autospec=True) as mock_data_dir:
        mock_data_dir.return_value = '/fake/data/dir/comp'
        assert prop.get_default() == '/fake/data/dir/comp'
        mock_data_dir.assert_called_once_with(appname='comp')

      # check it parses default
      with mock.patch('appdirs.user_data_dir', autospec=True) as mock_data_dir:
        mock_data_dir.return_value = '/fake/data/../data/dir/comp'
        assert prop.get_default() == '/fake/data/dir/comp'
        mock_data_dir.assert_called_once_with(appname='comp')

    def test_editor_word_wrap(self):
      prop = Config.get_settings()['editor-word-wrap']
      assert prop.get_name() == 'editor-word-wrap'
      assert prop.get_type() == bool
      assert prop.get_default() == False

    def test_save_on_navigation(self):
      prop = Config.get_settings()['save-on-navigation']
      assert prop.get_name() == 'save-on-navigation'
      assert prop.get_type() == bool
      assert prop.get_default() == True

    def test_style_background_color(self):
      prop = Config.get_settings()['style.background-color']
      assert prop.get_name() == 'style.background-color'
      assert prop.get_type() == str
      assert prop.get_default() == ""

    def test_style_foreground_color(self):
      prop = Config.get_settings()['style.foreground-color']
      assert prop.get_name() == 'style.foreground-color'
      assert prop.get_type() == str
      assert prop.get_default() == ""

    def test_style_code_color(self):
      prop = Config.get_settings()['style.code-color']
      assert prop.get_name() == 'style.code-color'
      assert prop.get_type() == str
      assert prop.get_default() == ""

    def test_style_body_width(self):
      prop = Config.get_settings()['style.body-width']
      assert prop.get_name() == 'style.body-width'
      assert prop.get_type() == int
      assert prop.get_default() == 0

    def test_style_code_indent_width(self):
      prop = Config.get_settings()['style.code-indent-width']
      assert prop.get_name() == 'style.code-indent-width'
      assert prop.get_type() == int
      default = prop.get_default()
      assert default >= 0 and default < 100
      assert isinstance(default, int)

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  @mock.patch('appdirs.user_data_dir', autospec=True)
  def test_get_database_directory(self, mock_data_dir, mock_read):
    mock_data_dir.return_value = '/test/data/directory'
    expected = '/test/data/directory/database'

    Config.initialize()
    assert Config.get().get_database_dir() == expected

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  @mock.patch('appdirs.user_data_dir', autospec=True)
  def test_get_note_markdown_file(self, mock_data_dir, mock_read):
    mock_data_dir.return_value = '/test/data/directory'
    note_id = 11
    expected = '/test/data/directory/notes_markdown/11.md'

    Config.initialize()
    assert Config.get().get_note_markdown_file(note_id) == expected

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  @mock.patch('appdirs.user_data_dir', autospec=True)
  def test_get_record_filename(self, mock_data_dir, _):
    mock_data_dir.return_value = '/test/data/directory'
    table_name = 'my_model'
    model_id = 32
    expected = '/test/data/directory/database/my_model/32.info'

    Config.initialize()
    assert Config.get().get_record_filename(table_name, model_id) == expected

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  @mock.patch('appdirs.user_data_dir', autospec=True)
  def test_get_record_list_filename(self, mock_data_dir, _):
    mock_data_dir.return_value = '/test/data/directory'
    table_name = 'my_model'
    expected = '/test/data/directory/database/my_model/master.record'

    Config.initialize()

    assert Config.get().get_record_list_filename(table_name) == expected


class TestSettingBinding(object):

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  def test_basic(self, _):
    Config.initialize()
    pb = SettingBinding('_test-setting', str)
    assert pb.get_setting_name() == '_test-setting'
    assert pb.get_setting_value() == 'default-value'

  def test_init_error(self):
    with raises(NotInitializedError):
      pb = SettingBinding('_test-setting', str)

  @mock.patch('comp.file_op.read_file', autospec=True)
  def test_value_from_file(self, mock_read):
    mock_read.return_value = json.dumps({
      'normal': {
        '_test-setting': 'new value'
      }
    })
    Config.initialize()
    pb = SettingBinding('_test-setting', str)
    assert pb.get_setting_value() == 'new value'

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  def test_invalid_setting_name(self, _):
    Config.initialize()
    with raises(UnknownSettingError):
      SettingBinding('bad-setting', str)

  @mock.patch('comp.file_op.read_file', autospec=True, return_value=json.dumps({}))
  def test_invalid_setting_type(self, _):
    Config.initialize()
    with raises(SettingTypeError):
      SettingBinding('_test-setting', int)
