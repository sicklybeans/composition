import copy
import re
import warnings
from typing import Any, Type, Dict, Union
from pytest import raises, fixture
from unittest.mock import Mock, MagicMock, patch

import comp.constants
from tests.mock_base_model_writer import mock_base_model_writer
from comp.base_model import BaseModel, Database, NotRegisteredError, Field, LoadingStrategy, ReadOnlyError, ValidationError
from comp.models import Node, Note, Folder, register_all, unregister_all, MultipleOwnershipError, SelfOwnershipError, NoteConnector


TableClass = Type[BaseModel]
ID = int
JSONDict = Dict[str, Any]

unregister_all()  # don't remove

@fixture(autouse=True)
def global_setup_teardown():
  comp.config.Config.initialize()
  register_all()
  yield

  # ignore 'test only method' warning from `deinitialize`
  with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    comp.config.Config.deinitialize()

  with warnings.catch_warnings():
    warnings.simplefilter('ignore')
    unregister_all()

  Database.reset()


@patch('comp.base_model.BaseModelWriter', autospec=True)
def initialize_database_with(database_data: Dict[TableClass, Dict[ID, JSONDict]], mock_writer):
  mock_base_model_writer(mock_writer, database_data)
  Database.initialize()  # loads master record list

class TestNode(object):

  def test_cannot_init(self):
    Database.initialize()
    with raises(NotRegisteredError):
      Node()
    assert not Node.is_registered()

  def test_correct_cols(self):
    cols = Node.get_columns()
    assert len(cols) == 1
    assert cols['title'] == Field(str, required=True)

class TestNote(object):

  def test_correct_cols(self):
    cols = Note.get_columns()
    assert len(cols) == 3
    assert cols['title'] == Field(str, required=True)
    assert cols['created_date'] == Field(float, read_only=True, required=True)
    assert cols['modified_date'] == Field(float, read_only=True, required=True)

  @patch('time.time', autospec=True)
  @patch('comp.models.NoteConnector', autospec=True)
  def test_init_with_defaults(self, mock_note_connector, mock_time):
    initialize_database_with({})
    mock_time.return_value = 12.34

    n = Note()
    assert n.id == 1
    assert n.title == 'Untitled note'
    assert n.created_date == 12.34
    assert n.modified_date == 12.34
    assert not n.is_modified()
    mock_note_connector.write_markdown_file.assert_called_once()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_load_missing_title_error(self, mock_writer):
    mock_base_model_writer(mock_writer, {
      Note: {1: {
        'created_date': 1234.1,
        'modified_date': 1234.2
      }}
    })
    with raises(AttributeError):
      Database.initialize()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_load_missing_created_date_error(self, mock_writer):
    mock_base_model_writer(mock_writer, {
      Note: {1: {
        'title': 'has title',
        'modified_date': 1234.2
      }}
    })
    with raises(AttributeError):
      Database.initialize()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_load_missing_modified_date_error(self, mock_writer):
    mock_base_model_writer(mock_writer, {
      Note: {1: {
        'title': 'has title',
        'created_date': 1234.1,
      }}
    })
    with raises(AttributeError):
      Database.initialize()

  @patch('comp.base_model.BaseModelWriter', autospec=True)
  def test_load_normal(self, mock_writer):
    mock_base_model_writer(mock_writer, {
      Note: {1: {
        'title': 'has title',
        'created_date': 1234.1,
        'modified_date': 1234.2
      }}
    })
    Database.initialize()
    assert Note.get(1).title == 'has title'
    assert Note.get(1).created_date == 1234.1
    assert Note.get(1).modified_date == 1234.2

  @patch('comp.models.NoteConnector', autospec=True)
  def test_load_immediate(self, mock_note_connector):
    assert Note.get_loading_strategy() == LoadingStrategy.IMMEDIATE

    # We check that initializing database automatically loads all instances into memory
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
      2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4}
    }})

    assert Note.has_model_instance(1)
    assert Note.has_model_instance(2)
    assert Note.get(1).title == 'note 1'

  @patch('comp.models.NoteConnector', autospec=True)
  def test_creates_markdown_only_for_new_note(self, mock_note_connector):
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
      2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4}
    }})
    mock_note_connector.write_markdown_file.assert_not_called()
    mock_note_connector.read_markdown_file.assert_not_called()
    mock_note_connector.remove_markdown_file.assert_not_called()

  @patch('comp.models.NoteConnector', autospec=True)
  def test_new_note_default_title(self, mock_note_connector):
    initialize_database_with({Note: {1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}}})

    new_note = Note()
    assert new_note.title == 'Untitled note'
    assert new_note.id == 2

  @patch('comp.models.NoteConnector', autospec=True)
  def test_new_note_creates_file(self, mock_note_connector):
    initialize_database_with({Note: {1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}}})

    new_note = Note()
    mock_note_connector.write_markdown_file.assert_called_once_with(new_note, '# %s\n' % new_note.title)

    mock_note_connector.write_markdown_file.reset_mock()

    new_note = Note(title='my new note')
    mock_note_connector.write_markdown_file.assert_called_once_with(new_note, '# my new note\n')

  @patch('comp.models.NoteConnector', autospec=True)
  def test_remove_removes_markdown_file(self, mock_note_connector):
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
      2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4}
    }})
    mock_note_connector.remove_markdown_file.assert_not_called()

    note = Note.get(1)
    note.remove()
    mock_note_connector.write_markdown_file.assert_not_called()
    mock_note_connector.remove_markdown_file.assert_called_once_with(note)
    assert Note.get_removed() == {note}

  @patch('comp.models.NoteConnector', autospec=True)
  def test_remove_removes_added_file(self, mock_note_connector):
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}
    }})
    mock_note_connector.remove_markdown_file.assert_not_called()

    note = Note()
    mock_note_connector.write_markdown_file.assert_called_once()
    note.remove()
    mock_note_connector.remove_markdown_file.assert_called_once_with(note)

  @patch('comp.models.NoteConnector', autospec=True)
  def test_new_note_sets_dates_to_now(self, mock_note_connector):
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}
    }})
    mock_note_connector.remove_markdown_file.assert_not_called()
    mock_time = None

    with patch('time.time', return_value=1234.5678):
      note = Note()
      assert note.created_date == 1234.5678
      assert note.modified_date == 1234.5678

  @patch('comp.models.NoteConnector', autospec=True)
  def test_changing_note_title_sets_modified_date(self, mock_note_connector):
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 1.2}
    }})
    note = Note.get(1)
    assert not note.is_modified()

    assert note.modified_date == 1.2
    with patch('time.time', return_value=3.4):
      note.title = 'my new title'
    assert note.modified_date == 3.4
    with patch('time.time', return_value=5.6):
      note.title = 'my newer title'
    assert note.modified_date == 3.4 # still original value of 3.4!

    assert note.created_date == 1.2
    assert note.is_modified()

  @patch('comp.models.NoteConnector', autospec=True)
  def test_dates_are_read_only(self, mock_note_connector):
    initialize_database_with({Note: {
      1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}
    }})
    note = Note.get(1)
    with raises(ReadOnlyError):
      note.modified_date = 5.8
    with raises(ReadOnlyError):
      note.created_date = 5.8
    assert note.created_date == 1.2
    assert note.modified_date == 3.4
    assert not note.is_modified()

  @patch('comp.models.NoteConnector', autospec=True)
  def test_remove_note_removes_from_parent(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [1, 2]}
      }
    })

    assert Folder.get(1).child_note_ids == (1, 2)
    Note.get(2).remove()
    assert Folder.get(1).child_note_ids == (1,)

  def test_get_parent_id(self):
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [4,5]},
        2: {'title': 'folder 2', 'child_folder_ids': [], 'child_note_ids': []},
        3: {'title': 'folder 3', 'child_folder_ids': [], 'child_note_ids': []},
      },
      Note: {
        4: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        5: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
        6: {'title': 'note 3', 'created_date': 1.2, 'modified_date': 3.4}
      },
    })
    assert Note.get(4).get_parent_id() == 1
    assert Note.get(5).get_parent_id() == 1
    assert Note.get(6).get_parent_id() == None

    Note.get(5).change_parent(2)

    assert Note.get(4).get_parent_id() == 1
    assert Note.get(5).get_parent_id() == 2
    assert Note.get(6).get_parent_id() == None

    Folder.get(1).child_note_ids = (6,)
    Folder.get(3).child_note_ids = (4,)

    assert Note.get(4).get_parent_id() == 3
    assert Note.get(5).get_parent_id() == 2
    assert Note.get(6).get_parent_id() == 1

@patch('comp.models.NoteConnector', autospec=True)
class TestFolder(object):

  def test_init_with_defaults(self, mock_note_connector):
    initialize_database_with({})

    f = Folder()
    assert f.id == 1
    assert f.title == 'Untitled folder'
    assert f.child_folder_ids == tuple()
    assert f.child_note_ids == tuple()
    assert not f.is_modified()

  def test_add_child_note(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
        3: {'title': 'note 3', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Folder.get(1).child_note_ids = (1, 2)
    with patch('comp.base_model.BaseModelWriter', autospec=True) as mock_writer:
      Database.commit_changes()
      mock_writer.write_record_file.assert_called_once_with(Folder, 1, {
        'title': 'folder 1',
        'child_note_ids': (1, 2),
        'child_folder_ids': tuple()
      })

  def test_add_pending_child_note(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    note = Note()
    f = Folder.get(1)
    f.child_note_ids = (1, note.id)
    with patch('comp.base_model.BaseModelWriter', autospec=True) as mock_writer:
      Database.commit_changes()
      assert mock_writer.write_record_file.call_count == 2
      mock_writer.write_record_file.assert_any_call(Folder, 1, {
        'title': 'folder 1',
        'child_note_ids': (1, 2),
        'child_folder_ids': tuple()
      })

  def test_add_removed_child_note_error(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Note.get(2).remove()
    f = Folder.get(1)
    f.child_note_ids = (1, 2)
    with raises(ValidationError):
      Database.commit_changes()

  def test_add_invalid_child_note_error(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Folder.get(1).child_note_ids = (1, 2)
    with raises(ValidationError):
      Database.commit_changes()

  def test_add_child_folder(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
        3: {'title': 'note 3', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
        2: {'title': 'folder 2', 'child_folder_ids': [], 'child_note_ids': []},
        3: {'title': 'folder 3', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Folder.get(1).child_folder_ids = (2,)
    assert Folder.get(1).is_modified()
    assert not Folder.get(2).is_modified()
    Folder.get(2).child_folder_ids = (3,)
    assert Folder.get(2).is_modified()
    assert not Folder.get(3).is_modified()
    with patch('comp.base_model.BaseModelWriter', autospec=True) as mock_writer:
      Database.commit_changes()
      print(mock_writer.write_record_file.mock_calls)
      assert mock_writer.write_record_file.call_count == 2
      mock_writer.write_record_file.assert_any_call(Folder, 1, {
        'title': 'folder 1',
        'child_note_ids': tuple(),
        'child_folder_ids': (2,)
      })
      mock_writer.write_record_file.assert_any_call(Folder, 2, {
        'title': 'folder 2',
        'child_note_ids': tuple(),
        'child_folder_ids': (3,)
      })

  def test_add_pending_child_folder(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Folder.get(1).child_folder_ids = (Folder().id,)
    with patch('comp.base_model.BaseModelWriter', autospec=True) as mock_writer:
      Database.commit_changes()
      assert mock_writer.write_record_file.call_count == 2
      mock_writer.write_record_file.assert_any_call(Folder, 1, {
        'title': 'folder 1',
        'child_note_ids': tuple(),
        'child_folder_ids': (2,)
      })

  def test_add_removed_child_folder_error(self, mock_note_connector):
    initialize_database_with({
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4}
      },
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
        2: {'title': 'folder 2', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Folder.get(2).remove()
    Folder.get(1).child_folder_ids = (2,)
    with raises(ValidationError):
      Database.commit_changes()

  def test_add_invalid_child_folder_error(self, mock_note_connector):
    initialize_database_with({
      Note: {},
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      }
    })

    Folder.get(1).child_folder_ids = (2,)
    with raises(ValidationError):
      Database.commit_changes()

  def test_get_parent_id(self, mock_note_connector):
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [2,3], 'child_note_ids': []},
        2: {'title': 'folder 2', 'child_folder_ids': [], 'child_note_ids': []},
        3: {'title': 'folder 3', 'child_folder_ids': [], 'child_note_ids': []},
        4: {'title': 'folder 3', 'child_folder_ids': [], 'child_note_ids': []},
      },
    })
    assert Folder.get(1).get_parent_id() == None
    assert Folder.get(2).get_parent_id() == 1
    assert Folder.get(3).get_parent_id() == 1
    assert Folder.get(4).get_parent_id() == None

    Folder.get(3).change_parent(2)

    assert Folder.get(1).get_parent_id() == None
    assert Folder.get(2).get_parent_id() == 1
    assert Folder.get(3).get_parent_id() == 2
    assert Folder.get(4).get_parent_id() == None

    Folder.get(2).child_folder_ids = tuple()
    Folder.get(1).child_folder_ids = (3,4)
    Folder.get(3).child_folder_ids = (2,)

    assert Folder.get(1).get_parent_id() == None
    assert Folder.get(2).get_parent_id() == 3
    assert Folder.get(3).get_parent_id() == 1
    assert Folder.get(4).get_parent_id() == 1


class TestFolderChild(object):

  @patch('comp.models.NoteConnector', autospec=True)
  class TestAtLoad(object):
    """
    Tests that invalid folder/child interactions are caught when loading data.
    """

    def test_multiple_ownership_error_folder(self, mock_note_connector):
      with raises(MultipleOwnershipError):
        initialize_database_with({
          Folder: {
            1: {'title': 'folder 1', 'child_folder_ids': [3], 'child_note_ids': []},
            2: {'title': 'folder 1', 'child_folder_ids': [3], 'child_note_ids': []},
            3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
          },
        })

    def test_multiple_ownership_error_notes(self, mock_note_connector):
      with raises(MultipleOwnershipError):
        initialize_database_with({
          Folder: {
            1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [1]},
            2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [1]},
          },
          Note: {
            1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
          },
        })

    def test_self_ownership_error(self, mock_note_connector):
      with raises(SelfOwnershipError):
        initialize_database_with({
          Folder: {
            1: {'title': 'folder 1', 'child_folder_ids': [1,], 'child_note_ids': []},
          },
        })

    def test_circular_ownership_error(self, mock_note_connector):
      with raises(SelfOwnershipError):
        initialize_database_with({
          Folder: {
            1: {'title': 'folder 1', 'child_folder_ids': [2], 'child_note_ids': []},
            2: {'title': 'folder 1', 'child_folder_ids': [3], 'child_note_ids': []},
            3: {'title': 'folder 1', 'child_folder_ids': [1], 'child_note_ids': []}
          },
        })


  @patch('comp.models.NoteConnector', autospec=True)
  class TestSetAttr(object):
    """
    Tests that invalid folder/child interactions are caught when setting fields.
    """

    def test_multiple_ownership_error_folder(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
        },
      })
      Folder.get(1).child_folder_ids = (3,)
      with raises(MultipleOwnershipError):
        Folder.get(2).child_folder_ids = (3,)

    def test_multiple_ownership_error_notes(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
        },
        Note: {
          1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        },
      })
      Folder.get(1).child_note_ids = (1,)
      with raises(MultipleOwnershipError):
        Folder.get(2).child_note_ids = (1,)

    def test_reassigning_folders_okay(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 2', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 3', 'child_folder_ids': [], 'child_note_ids': []},
          4: {'title': 'folder 4', 'child_folder_ids': [], 'child_note_ids': []},
        },
      })

      Folder.get(1).child_folder_ids = (3,4)
      Folder.get(1).child_folder_ids = (2,3)
      Folder.get(2).child_folder_ids = (4,)

    def test_self_ownership_error(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
        },
      })
      with raises(SelfOwnershipError):
        Folder.get(1).child_folder_ids = (1,)

    def test_circular_ownership_error(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
        },
      })
      Folder.get(1).child_folder_ids = (2,)
      Folder.get(2).child_folder_ids = (3,)
      with raises(SelfOwnershipError):
        Folder.get(3).child_folder_ids = (1,)

    def test_circular_ownership_error_out_of_order(self, mock_note_connector):
      """
      I didnt think to test this separately from above but there was a bug and this test is designed
      to catch that bug.
      """
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 2', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 3', 'child_folder_ids': [], 'child_note_ids': []},
        },
      })
      Folder.get(1).child_folder_ids = (2,)
      Folder.get(3).child_folder_ids = (1,)
      with raises(SelfOwnershipError):
        Folder.get(2).child_folder_ids = (3,)

  @patch('comp.models.NoteConnector', autospec=True)
  class TestInit(object):
    """
    Tests that invalid folder/child interactions are caught when creating new objects.
    """

    def test_multiple_ownership_error_folder(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
        },
      })

      Folder.get(1).child_folder_ids = (3,)
      Folder(child_folder_ids=(1,)) # okay
      with raises(MultipleOwnershipError):
        Folder(child_folder_ids=(3,))

    def test_multiple_ownership_error_note(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
        },
      })

      note = Note()
      Folder.get(1).child_note_ids = (note.id,)
      with raises(MultipleOwnershipError):
        Folder(child_note_ids=(note.id,))

    def test_self_ownership_error(self, mock_note_connector):
      initialize_database_with({
        Folder: {
          1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
          3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
        },
      })

      with raises(SelfOwnershipError):
        Folder(child_folder_ids=(4,))


  @patch('comp.models.NoteConnector', autospec=True)
  def test_removing_child_note_propagates_up(self, mock_note_connector):
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [2,3], 'child_note_ids': [1,2]},
        2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [3]},
        3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []}
      },
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
        3: {'title': 'note 3', 'created_date': 1.2, 'modified_date': 3.4}
      },
    })

    assert not Folder.get_modified()
    assert Folder.get(1).child_note_ids == (1, 2)
    assert Folder.get(2).child_note_ids == (3,)
    Note.get(1).remove()
    assert Folder.get_modified() == {Folder.get(1)}
    assert Folder.get(1).child_note_ids == (2,)
    assert Folder.get(2).child_note_ids == (3,)

  @patch('comp.models.NoteConnector', autospec=True)
  def test_removing_child_folder_propagates_up(self, mock_note_connector):
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [2,3], 'child_note_ids': [1,2]},
        2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
        3: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': []},
      },
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
      },
    })

    assert not Folder.get_modified()
    assert Folder.get(1).child_folder_ids == (2, 3)
    assert Folder.get(2).child_folder_ids == tuple()
    Folder.get(3).remove()
    assert Folder.get_modified() == {Folder.get(1)}
    assert Folder.get(1).child_folder_ids == (2,)
    assert Folder.get(2).child_folder_ids == tuple()

  @patch('comp.models.NoteConnector', autospec=True)
  def test_change_folder_parent(self, mock_note_connector):
    # setup ownership structure
    # Folder 1
    #   Folder 2
    #     Folder 3
    #       Folder 4
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [2], 'child_note_ids': []},
        2: {'title': 'folder 2', 'child_folder_ids': [3], 'child_note_ids': []},
        3: {'title': 'folder 3', 'child_folder_ids': [4], 'child_note_ids': []},
        4: {'title': 'folder 4', 'child_folder_ids': [], 'child_note_ids': []},
      }
    })
    assert not Folder.get_modified()
    Folder.get(3).change_parent(1)
    assert Folder.get(1).child_folder_ids == (2, 3)
    assert Folder.get(2).child_folder_ids == tuple()
    assert Folder.get(3).child_folder_ids == (4,)
    assert Folder.get_modified() == {Folder.get(1), Folder.get(2)}

  @patch('comp.models.NoteConnector', autospec=True)
  def test_change_folder_parent_to_none(self, mock_note_connector):
    # setup ownership structure
    # Folder 1
    #   Folder 2
    #     Folder 3
    #       Folder 4
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [2], 'child_note_ids': []},
        2: {'title': 'folder 2', 'child_folder_ids': [3], 'child_note_ids': []},
        3: {'title': 'folder 3', 'child_folder_ids': [4], 'child_note_ids': []},
      }
    })
    assert not Folder.get_modified()
    Folder.get(2).change_parent(None)
    assert Folder.get(1).child_folder_ids == tuple()
    assert Folder.get(2).child_folder_ids == (3,)
    assert Folder.get_modified() == {Folder.get(1)}

  @patch('comp.models.NoteConnector', autospec=True)
  def test_change_folder_parent_circular(self, mock_note_connector):
    # setup ownership structure
    # Folder 1
    #   Folder 2
    #     Folder 3
    #       Folder 4
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [2], 'child_note_ids': []},
        2: {'title': 'folder 2', 'child_folder_ids': [3], 'child_note_ids': []},
        3: {'title': 'folder 3', 'child_folder_ids': [4], 'child_note_ids': []},
        4: {'title': 'folder 4', 'child_folder_ids': [], 'child_note_ids': []},
      }
    })

    with raises(SelfOwnershipError):
      Folder.get(2).change_parent(4)

  @patch('comp.models.NoteConnector', autospec=True)
  def test_change_note_parent(self, mock_note_connector):
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [1, 2]},
        2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [3, 4]},
      },
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
        3: {'title': 'note 3', 'created_date': 1.2, 'modified_date': 3.4},
        4: {'title': 'note 4', 'created_date': 1.2, 'modified_date': 3.4},
      },
    })

    Note.get(1).change_parent(2)
    assert Folder.get_modified() == {Folder.get(1), Folder.get(2)}
    assert not Note.get_modified()
    assert Folder.get(1).child_note_ids == (2,)
    assert Folder.get(2).child_note_ids == (3,4,1)

  @patch('comp.models.NoteConnector', autospec=True)
  def test_change_note_parent_none(self, mock_note_connector):
    initialize_database_with({
      Folder: {
        1: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [1]},
        2: {'title': 'folder 1', 'child_folder_ids': [], 'child_note_ids': [2]},
      },
      Note: {
        1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4},
        2: {'title': 'note 2', 'created_date': 1.2, 'modified_date': 3.4},
      },
    })

    Note.get(1).change_parent(None)
    assert Folder.get_modified() == {Folder.get(1)}
    assert not Note.get_modified()
    assert Folder.get(1).child_note_ids == tuple()
    assert Folder.get(2).child_note_ids == (2,)

class TestNoteConnector(object):

  @patch('comp.file_op.read_file', autospec=True)
  @patch('comp.config.Config.get_note_markdown_file', autospec=True)
  def test_read_markdown_file_basic(self, mock_get_markdown_file, mock_read_file):
    initialize_database_with({
      Note: {1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}}
    })
    mock_get_markdown_file.return_value = 'markdown/file.md'
    mock_read_file.return_value = 'file contents'

    assert NoteConnector.read_markdown_file(Note.get(1)) == 'file contents'

    mock_get_markdown_file.assert_called_once_with(comp.config.Config.get(), 1)
    mock_read_file.assert_called_once_with('markdown/file.md')

  @patch('comp.file_op.write_file', autospec=True)
  @patch('comp.config.Config.get_note_markdown_file', autospec=True)
  def test_write_markdown_file_basic(self, mock_get_markdown_file, mock_write_file):
    initialize_database_with({
      Note: {1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}}
    })
    mock_get_markdown_file.return_value = 'markdown/file.md'

    NoteConnector.write_markdown_file(Note.get(1), 'file contents')

    mock_get_markdown_file.assert_called_once_with(comp.config.Config.get(), 1)
    mock_write_file.assert_called_once_with('markdown/file.md', 'file contents')

  @patch('comp.file_op.rename_to_temp', autospec=True)
  @patch('comp.config.Config.get_note_markdown_file', autospec=True)
  def test_remove_markdown_file_basic(self, mock_get_markdown_file, mock_rename):
    initialize_database_with({
      Note: {1: {'title': 'note 1', 'created_date': 1.2, 'modified_date': 3.4}}
    })
    mock_get_markdown_file.return_value = 'markdown/file.md'

    NoteConnector.remove_markdown_file(Note.get(1))

    mock_get_markdown_file.assert_called_once_with(comp.config.Config.get(), 1)
    mock_rename.assert_called_once_with('markdown/file.md', ignore_missing=True)
